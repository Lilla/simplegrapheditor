package graphEditor;

import java.awt.Graphics;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Iterator;
import java.io.FileOutputStream;

//Lilla Lomnicka 238306

public class Graph implements Serializable{

	private static final long serialVersionUID = 1L;
	
	 private Set<Node> nodes;
	 private Set<Edge> edges;
	
	public  Graph() {
		this.nodes=new HashSet<Node>();
		this.edges=new HashSet<Edge>();
	}

	public void addNode(Node node) {
		nodes.add(node);
	}
	public void addEdge(Edge edge) {
		edges.add(edge);
	}
	
	public void removeEdge(Edge edge) {
		 edges.remove(edge);
	}
	
	public void romoveNode(Node node) {
		 Iterator<Edge> iter = edges.iterator();
		    lab: for (; iter.hasNext(); 
		        
		        iter.remove())
		    {
		      Edge edge = (Edge)iter.next();
		      if ((edge.getStartNode() != node) && (edge.getEndNode() != node)) break lab;
		    }
		    nodes.remove(node);
		  }
	public void romoveNode(ColorNode node) {
		 Iterator<Edge> iter = edges.iterator();
		    lab: for (; iter.hasNext(); 
		        
		        iter.remove())
		    {
		      Edge edge = (Edge)iter.next();
		      if ((edge.getStartNode() != node) && (edge.getEndNode() != node)) break lab;
		    }
		    nodes.remove(node);
		  }
	
	
	public Node[] getNode(){
		Node[] array=new Node[0]; 
		return nodes.toArray(array); 
		
	}
	public Edge[] getEdge(){
		Edge[] array=new Edge[0]; 
		return edges.toArray(array); 
		
	}
	
	public void draw(Graphics g) {
		for(Node node: nodes) {
			node.draw(g);
		}
		for(Edge edge: edges) {
			edge.draw(g);
		}
	}
	
	 public static void writeToFile(String file, Graph graph) throws Exception {
		     try {
		      
		      try { ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
		      outputStream.writeObject(graph);
		      } catch (FileNotFoundException e)
		    { throw new Exception("File not found <" + file + ">");
		    } }catch (IOException e) 
		    {
		      throw new Exception("Error <" + file + ">");
		    }
		  
		}
	 
/*	 public static void readFromFile(String fileName) {
		 InputStream file = new FileInputStream(fileName);
	      InputStream buffer = new BufferedInputStream(file);
	      ObjectInput input = new ObjectInputStream (buffer);
	    ){
	      //deserialize the List
	      List<String> recoveredQuarks = (List<String>)input.readObject();
}*/
	 }