package graphEditor;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;
//Lilla Lomnicka 238306

public class Node implements Serializable{
	
	//wspolrzedne kola 
	protected int x;
	protected int y; 
	
	protected int r; //promien kolo

	
	//konstruktor 
	public Node(int x, int y) {
		this.x=x;
		this.y=y;
		this.r=10; 
		}
	//gettery
	public int getX() {
		return x; 
	}
	
	public int getY() {
		return y; 
	}

	public	int getR() {
		return r; 
	}
	


	//settery
	public void setX(int x) {
		this.x=x; 
	}
	
	public void setY(int y) {
		this.y=y; 
	}

	public	void setR(int r) {
		this.r=r; 
	}
	
	
public boolean isMouseOver(int mouseX, int mouseY) {
		return (x-mouseX)*(x-mouseX)+(y-mouseY)*(y-mouseY)<=r*r;
	}

void draw(Graphics g) {
    g.setColor(Color.BLACK);
    g.fillOval(x - 3, y - 3, 6, 6);
  }

@Override 
public String toString( ) {
	return  "(" + x +", " + y + ", " + r + " "+ ")";
}
}
