package graphEditor;

import java.awt.Color;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.lang.Exception;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class GraphEditor extends JFrame implements ActionListener{

	
	private static final long serialVisionUID=1L;
	
	private final static String APP_AUTHOR="Author: Lila Lomnicka \n Date: december'18";
	private final static String APP_TITLE="Simple graph editor";
	
	private final static String APP_INSTRICTION="                   Program pozwala na                         "
			+ "\n\nAktywne klawisze:\n   strzałki ==> przesuwanie całego grafu\n  "
			+ " SHIFT + strzałki ==> szybkie przesuwanie całego grafu\n\n gdy kursor wskazuje węzeł/krawędź:\n "
			+ "  Back space   ==> kasowanie węzła/krawędzi\n "
			+ "  w,r,g,b,c,m,y,p ==> zmiana koloru węzła\n\n\n"
			+ "Operacje myszka:\n"
			+ " Lewy przycisk ==> przesuwanie węzła i krawedzi\n  "
			+ " Prawy przycisk ==> tworzenie nowego węzła\n"
			+ "Prawy przycisk gdy kursor wskazuje wezel edycja wezłą, a gdy wskazuję krawędz, to edycję krawędzi\n "
			+"Nie dziala odczyt z pliku ";
		
		

public static void  main(String...args) {
	new GraphEditor();}
	
	private JMenuBar menuBar=new JMenuBar();
	private JMenu menuFile=new JMenu("File");
	private JMenu menuGraph=new JMenu("Graph");
	private JMenu menuHelp=new JMenu("Help");
	private JMenuItem menuNew=new JMenuItem("New node", KeyEvent.VK_N);
	private JMenuItem menuShowExample= new JMenuItem("Example", KeyEvent.VK_X);
	private JMenuItem menuExit= new JMenuItem("Exit", KeyEvent.VK_E);
	private JMenuItem menuListOfNodes= new JMenuItem("List of nodes", KeyEvent.VK_N);
	private JMenuItem menuAuthor= new JMenuItem("Author", KeyEvent.VK_A);
	private JMenuItem menuInstruction= new JMenuItem("Instruction", KeyEvent.VK_I);
	private JMenuItem menuListOfEdges= new JMenuItem("List of edges", KeyEvent.VK_N);
	private JMenuItem menuWriteToFile= new JMenuItem("Write to file", KeyEvent.VK_Q);
	private JMenuItem menuReadFromFile= new JMenuItem("Read from file", KeyEvent.VK_B);
	private graphPanel panel=new graphPanel();
	
	public GraphEditor() {
		super(APP_TITLE);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(400, 500);
		
		
		setContentPane(panel);
		createMenu();
		setLocationRelativeTo(null);
		
		showExample();
		setVisible(true);
		
	}
	
	private void showListofNodes(Graph graph) {
		Node array[]=graph.getNode();
		int i=0;
		StringBuilder message=new StringBuilder("Amound of nodes: "+array.length+"\n");
		for(Node node: array) {
			message.append(node+" ");
			if(++i%5==0)
				message.append("\n");
		}
		JOptionPane.showMessageDialog(this, message, APP_TITLE +"-list of nodes", JOptionPane.PLAIN_MESSAGE);
		
	}
	
	  private void showListOfEdges(Graph graph) {
		    Edge[] array = graph.getEdge();
		    int i = 0;
		    StringBuilder message = new StringBuilder("Amoud of edges: " + array.length + "\n");
		    for (Edge edge : array) {
		      message.append(edge + "    ");
		      i++; if (i % 3 == 0) message.append("\n");
		    }
		    JOptionPane.showMessageDialog(this, message, "List of edges", -1);
		  }
	
	private void createMenu() {
		menuNew.addActionListener(this);
		menuFile.addActionListener(this);
		menuGraph.addActionListener(this);
		menuHelp.addActionListener(this);
		menuNew.addActionListener(this);
		menuShowExample.addActionListener(this);
		menuExit.addActionListener(this);
		menuListOfNodes.addActionListener(this);
		menuAuthor.addActionListener(this);
		menuInstruction.addActionListener(this);
	    menuListOfEdges.addActionListener(this);
	    menuWriteToFile.addActionListener(this);
	    menuReadFromFile.addActionListener(this);
		
	    
	    menuFile.setMnemonic(KeyEvent.VK_G);
	    menuFile.add(menuNew);
	    menuFile.add(menuShowExample);
	    menuFile.addSeparator();
	    menuFile.add(menuReadFromFile);
	    menuFile.add(menuWriteToFile);
	    menuFile.addSeparator();
		
		menuFile.add(menuExit);
		menuGraph.setMnemonic(KeyEvent.VK_G);
	    menuGraph.add(menuListOfNodes);
		menuGraph.add(menuListOfEdges);
		
		
		menuHelp.setMnemonic(KeyEvent.VK_G);
		menuHelp.add(menuInstruction);
		menuHelp.add(menuAuthor);
		
		menuBar.add(menuFile);
		menuBar.add(menuGraph);
		menuBar.add(menuHelp);
		setJMenuBar(menuBar);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source=e.getSource();
		
		if(source==menuNew) {
			panel.setGraph(new Graph());
		}
		if(source==menuShowExample) {
			
			showExample();
		}
	
	  if(source==menuWriteToFile) {
		   String file = JOptionPane.showInputDialog("File name");
		      if ((file == null) || (file.equals(""))) return;
		      try {
		        Graph.writeToFile(file, panel.getGraph());
		      } catch (Exception ex) {
		        JOptionPane.showMessageDialog(this, ex.getMessage(), "error", 0);
		      }
	}
		if(source==menuListOfEdges) {
			showListOfEdges(panel.getGraph());
		}
		if(source==menuListOfNodes) {
			showListofNodes(panel.getGraph());
		}
		if(source==menuAuthor) {
			JOptionPane.showMessageDialog(this, APP_AUTHOR, APP_TITLE, JOptionPane.INFORMATION_MESSAGE);
		}
		if(source==menuInstruction) {
			JOptionPane.showMessageDialog(this, APP_INSTRICTION, APP_TITLE, JOptionPane.INFORMATION_MESSAGE);
		}
		if(source==menuExit) {
			System.exit(0);
		}
		
	}
	
	private void showExample() {
		
		  {
		    Graph graph = new Graph();
		    
		    ColorNode node1 = new ColorNode(130, 70, "A", Color.CYAN);
		    ColorNode node2 = new ColorNode(220, 220, "B", Color.GREEN);
		    ColorNode node3 = new ColorNode(100, 300, "C", Color.RED);
		    Node node4 = new Node(300, 120);
		    Node node5 = new Node(200, 120);
		    Node node6 = new Node(300, 260);
		    ColorNode node7 = new ColorNode(300, 70, "C", Color.BLUE);
		    
		    graph.addNode(node1);
		    graph.addNode(node2);
		    graph.addNode(node3);
		    graph.addNode(node4);
		    graph.addNode(node5);
		    graph.addNode(node6);
		    graph.addNode(node7);
		    

		    graph.addEdge(new ColorEdge(node1, node2, Color.YELLOW));
		    graph.addEdge(new ColorEdge(node2, node3, Color.BLUE));
		    graph.addEdge(new ColorEdge(node1, node3, Color.CYAN));
		    graph.addEdge(new Edge(node4, node2));
		    graph.addEdge(new Edge(node2, node5));
		    graph.addEdge(new Edge(node4, node5));
		    
		    panel.setGraph(graph);
		  }
	}

}
