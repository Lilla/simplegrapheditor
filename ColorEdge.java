package graphEditor;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
//Lilla Lomnicka 238306

public class ColorEdge extends Edge {

	protected Color color; 
	public ColorEdge(Node startNode, Node endNode) {
		super(startNode, endNode);
		 this.color=Color.WHITE;
	}
	
	public ColorEdge(Node startNode, Node endNode, Color color ) {
		super(startNode, endNode);
		this.color=color; 
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color=color;
	}
	void draw(Graphics g) {
		g.setColor(color);
		 ((Graphics2D)g).setStroke(new BasicStroke(3.0F));
		g.drawLine(startNode.getX(), startNode.getY(), endNode.getX(), endNode.getY());
		 ((Graphics2D)g).setStroke(new BasicStroke(1.0F));
		 g.setColor(Color.BLACK);
	}
	
	

}
