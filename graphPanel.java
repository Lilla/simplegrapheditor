package graphEditor;

import java.awt.Color;
import java.awt.Point;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.PopupMenu;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

//Lilla Lomnicka 238306

public class graphPanel extends JPanel implements KeyListener, MouseListener, MouseMotionListener{

	private static final long serialVersionUID = 1L;
	
	
	protected Graph graph; 
	private int mouseX=0;
	private int mouseY=0;
	public boolean mouseLeftClick=false;
	public boolean mouseRightClick=false;
	protected int mouseCursor=Cursor.DEFAULT_CURSOR;
	protected boolean inDrag=false;
	int startX=-1;
	int startY=-1;
	protected Node nodeUnderCoursor=null; 
	protected Edge edgeUnderCoursor=null;
	protected Node createStartEdge=null; 
	private Color currentColor; 
	
	public graphPanel() {
		addMouseListener(this);
		addKeyListener(this);
		setFocusable(true);
		addMouseMotionListener(this);
		requestFocus();
		this.currentColor=Color.WHITE;
		}
private Node findNode(int mouseX, int mouseY) {
	for(Node node: graph.getNode()) {
		if(node.isMouseOver(mouseX, mouseY))
				return node;
	}
		return null;
}

private Node findNode(MouseEvent event) {
      return findNode(event.getX(), event.getY());
}

private Edge findEdge(int mouseX, int mouseY) {
	for(Edge edge: graph.getEdge()) {
		if(edge.isMouseOver(mouseX, mouseY))
			return edge;
	}
	return null; 
}


private Edge findEdge(MouseEvent event) {
	return findEdge(event.getX(), event.getY());
}
	public int getMouseCoursor() {
		return mouseCursor;
	}
	public void setMouseCoursor(MouseEvent event) {
		nodeUnderCoursor=findNode(event);
		edgeUnderCoursor=findEdge(event);
		if(nodeUnderCoursor!=null)
		{
			mouseCursor=Cursor.HAND_CURSOR;
		}
		else if(edgeUnderCoursor!=null) {
			mouseCursor=Cursor.CROSSHAIR_CURSOR;
		}
		else if(mouseLeftClick) {
			mouseCursor=Cursor.MOVE_CURSOR;
			}
		else {
			mouseCursor=Cursor.DEFAULT_CURSOR;
		}
		setCursor(Cursor.getPredefinedCursor(mouseCursor));
	
		
		
		
		
		mouseX=event.getX();
		mouseY=event.getY();
		
	}
	
	Graph getGraph() {
		return graph;
	}
	
	protected void setMouseCoursor() {
		
		nodeUnderCoursor=findNode(mouseX,mouseY);
		edgeUnderCoursor=findEdge(mouseX,mouseY);
		if(nodeUnderCoursor!=null)
		{
			mouseCursor=Cursor.HAND_CURSOR;
		}
		else if(edgeUnderCoursor!=null) {
			mouseCursor=Cursor.CROSSHAIR_CURSOR;
		}
		else if(mouseLeftClick) {
			mouseCursor=Cursor.MOVE_CURSOR;
			}
		else {
			mouseCursor=Cursor.DEFAULT_CURSOR;
		}

		setCursor(Cursor.getPredefinedCursor(mouseCursor));
		
		
	//	if(edgeUnderCoursor!=null)
	
	}
	
	private void moveNode(int dx, int dy, Node node) {
		node.setX(node.getX()+dx);
		node.setY(node.getY()+dy);
	}
	
	private void moveNode2(Node node, MouseEvent e) {
		node.setX(e.getX());
		node.setY(e.getY());
	}
	
	private void moveEdge(Edge edge, MouseEvent e, int startX, int startY) {
	   int endX, endY, deltaX, deltaY;
	  
	   endX=e.getX();
	   endY=e.getY();
	   
	   
	   deltaX=endX-startX;
	   deltaY=endY-startY;
	   
	   edge.startNode.setX(edge.startNode.getX()+deltaX);
	   edge.startNode.setY(edge.startNode.getY()+deltaY);
	   edge.endNode.setX(edge.endNode.getX()+deltaX);
	   edge.endNode.setY(edge.endNode.getY()+deltaY);
	
		
		
	
	}
	
	private void moveAllNodes(int dx, int dy) {
		for(Node node: graph.getNode()) {
			moveNode(dx, dy, node);
		}
	}
	
	
	private void moveAllNodes2(int startX, int startY, MouseEvent e) {
		
		 int endX, endY, deltaX, deltaY;
		 Point point=e.getPoint();
		   endX=(int) point.getX();
		   endY=(int) point.getY();
		   
		   
		   deltaX=endX-startX/100000;
		   deltaY=endY-startY/100000;
		for(Node node: graph.getNode()) {
			moveNode( deltaX, deltaY, node);
		}
	}
	public void setGraph(Graph graph) {
		this.graph = graph;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(graph==null) return;
		graph.draw(g);
		
	}


	
	   
 public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
		     char typedKey = e.getKeyChar();
			      if (this.nodeUnderCoursor != null) {
			         if (this.nodeUnderCoursor instanceof ColorNode) {
			            ColorNode node = (ColorNode)this.nodeUnderCoursor;
				switch (typedKey) {
				case 'p':
					node.setColor(Color.PINK);
					break;
				case 'b':
					node.setColor(Color.BLUE);
					break;
				case 'y':
					node.setColor(Color.YELLOW);
					break;
				case 'g':
		              node.setColor(Color.GRAY);
		              break;
		        case 'm':
		               node.setColor(Color.MAGENTA);
		               break;
		        case 'r':
		               node.setColor(Color.RED);
		               break;
		        case 'w':
		              node.setColor(Color.WHITE);
				case '+':
					int r=node.getR()+10;
					node.setR(r);
					break;
				case '-':
					 r=node.getR()-10;
					node.setR(r);
				
					break;
				case KeyEvent.VK_BACK_SPACE:
					graph.romoveNode(nodeUnderCoursor);
					break;
				}
			         }
				 this.repaint();
			     this.setMouseCoursor();}
			     
			     if (this.edgeUnderCoursor != null) {
			         if (this.edgeUnderCoursor instanceof ColorEdge) {
			            ColorEdge edge = (ColorEdge)this.edgeUnderCoursor;
			            switch(typedKey) {
			            case 'b':
			               edge.setColor(Color.BLACK);
			               break;
			            case 'p':
							edge.setColor(Color.PINK);
							break;
			            case 'c':
			               edge.setColor(Color.cyan);
			               break;
			            case 'g':
			               edge.setColor(Color.GRAY);
			               break;
			            case 'm':
			               edge.setColor(Color.MAGENTA);
			               break;
			            case 'r':
			               edge.setColor(Color.RED);
			               break;
			            case 'w':
			               edge.setColor(Color.WHITE);
			               break;
			            case 'y':
							edge.setColor(Color.YELLOW);
							break;
			            case KeyEvent.VK_BACK_SPACE:
							graph.removeEdge(edgeUnderCoursor);
							break;
			        
			            }
			         }
			
			repaint();
			setMouseCoursor();
		}
	   }

	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int distance; 
		
		if(e.isShiftDown()) distance=10; 
		else distance=1; 
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			moveAllNodes(-distance, 0);
			break;
		case KeyEvent.VK_RIGHT:
			moveAllNodes(distance, 0);
			break;
		case KeyEvent.VK_DOWN:
			moveAllNodes(0, -distance);
			break;
		case KeyEvent.VK_UP:
			moveAllNodes(0, distance);
			break;
		case KeyEvent.VK_DELETE:
			if(nodeUnderCoursor!=null) {
				graph.romoveNode(nodeUnderCoursor);
				nodeUnderCoursor=null;
			}
			break;
			
		}
		repaint();
		setMouseCoursor();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}


	
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	
			   Point p = e.getPoint();
			  
			    startX = p.x;
			    startY = p.y;
			    inDrag = true;
		
		
		if(e.getButton()==1)
			mouseLeftClick=false;

		if(e.getButton()==3)
			mouseRightClick=false;
	      setMouseCoursor(e);
	     
	    	
	    		 // moveNode2(nodeUnderCoursor, e);
	    		//  repaint();
	    	  
		
		
		 
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.getButton()==1)
			mouseLeftClick=false;

		if(e.getButton()==3)
			mouseRightClick=false;
		setMouseCoursor(e);

	    if(e.getButton()==3) {
			if(nodeUnderCoursor!=null) {
				if (this.nodeUnderCoursor instanceof ColorNode) {
				ColorNode node = (ColorNode)this.nodeUnderCoursor;
				 createPopupMenu(e, node);
				}
				else createPopupMenu(e, nodeUnderCoursor);
				}
			if(edgeUnderCoursor!=null) {
				if (this.edgeUnderCoursor instanceof ColorEdge) {
					ColorEdge edge = (ColorEdge)this.edgeUnderCoursor;
					 createPopupMenu(e, edge);
					}
					else createPopupMenu(e, edgeUnderCoursor);
					}
				}
			
	    
	
			else {
				createPopupMenu(e);
			}	
			
		}	
				
		
	
 

	

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		if(mouseLeftClick=true) {
			if(nodeUnderCoursor !=null) {
				moveNode2(nodeUnderCoursor, e);
			}else if (edgeUnderCoursor!=null) {
				Point p = e.getPoint();
			    // System.err.println("mouse drag to " + p);
			    
			int  curX = (edgeUnderCoursor.endNode.getX()+edgeUnderCoursor.startNode.getX())/2;
			int curY = (edgeUnderCoursor.endNode.getY()+edgeUnderCoursor.startNode.getY())/2;
			   
			
			moveEdge(edgeUnderCoursor,e, curX, curY);
				//moveAllNodes(e.getX(), e.getY());
			}
		/*else { 
			Point point =e.getPoint();
			int	curX=e.getX();
			int	curY=e.getY();
				
				moveAllNodes(curX, curY);
			}*/
		}
				
		
		
		
		repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
		//moveNode2(nodeUnderCoursor, e);
		setMouseCoursor(e);
	//	repaint();
	}
	
	

@Override
public void mouseClicked(MouseEvent e) {
	 
    this.setMouseCoursor(e);
    if (e.getButton() == 1) {
       if (this.createStartEdge != null && this.nodeUnderCoursor != null && this.nodeUnderCoursor != this.createStartEdge) {
          String[] typy = new String[]{"Black line", "Color line"};
          String typ = (String)JOptionPane.showInputDialog(this, "Choose type of the new edge:", "New Edge", 3, null, typy, (Object)null);
          if (typ != null) {
             if (typ.equals("Black line")) {
                this.graph.addEdge(new Edge(this.createStartEdge, this.nodeUnderCoursor));
             }

             if (typ.equals("Color line")) {
                this.graph.addEdge(new ColorEdge(this.createStartEdge, this.nodeUnderCoursor, this.currentColor));
             }
          }

          this.repaint();
       }

       this.createStartEdge = null;
    }

    this.setMouseCoursor(e);
 }

	
protected void createPopupMenu(MouseEvent event) {
   JPopupMenu popup = new JPopupMenu();
   JMenuItem menuItem = new JMenuItem("Create new node");
   menuItem.addActionListener((a) -> {
      String[] types = new String[]{"Black point", "Color node"};
      String type = (String)JOptionPane.showInputDialog(this, "Choose type of the new node?:", "New Node", 3, null, types, (Object)null);
      if (type != null) {
         if (type.equals("Black point")) {
            this.graph.addNode(new Node(event.getX(), event.getY()));
         }

         if (type.equals("Color node")) {
            this.graph.addNode(new ColorNode(event.getX(), event.getY(), "?", this.currentColor));
         }
      }

      this.repaint();
   });
   popup.add(menuItem);
   popup.show(event.getComponent(), event.getX(), event.getY());
}
protected void createPopupMenu(MouseEvent e, ColorNode node) {
	JMenuItem menuItem; 
	
	JPopupMenu popupMenu=new JPopupMenu();
	 if (node instanceof ColorNode) {
		
	 }
	menuItem=new JMenuItem("Change node color"); 
	
	menuItem.addActionListener((action)->{
		Color newColor=JColorChooser.showDialog(this,"Choose background color", node.getColor());
		if(newColor!=null) {
			node.setColor(newColor);
		}
		repaint();
	});
	
	popupMenu.add(menuItem);
	
	menuItem=new JMenuItem("Change name of node"); 
	
	menuItem.addActionListener((action)->{String name=JOptionPane.showInputDialog(this, "Typ name");
	if(name!=null) {
		node.setName(name);
	}
	repaint();
});
	
	popupMenu.add(menuItem);
	menuItem=new JMenuItem("Romove this node");
	
	menuItem.addActionListener((action)->{
		graph.romoveNode(node);
		repaint();
	});
	
	popupMenu.add(menuItem);
	
	
menuItem=new JMenuItem("Create new edge form this node"); 
	
	menuItem.addActionListener((action)->{
		this.createStartEdge=node;
		this.mouseCursor=3;
	repaint();
});
	
	popupMenu.add(menuItem);
	popupMenu.show(e.getComponent(), e.getX(), e.getY());
	
	
}

protected void createPopupMenu(MouseEvent e, Node node) {
	JMenuItem menuItem; 
	JPopupMenu popupMenu=new JPopupMenu();
menuItem=new JMenuItem("Create new edge form this node"); 
	
	menuItem.addActionListener((action)->{
		this.createStartEdge=node;
		this.mouseCursor=3;
	repaint();
});
	
	popupMenu.add(menuItem);
menuItem=new JMenuItem("Romove this node");
	
	menuItem.addActionListener((action)->{
		graph.romoveNode(node);
		repaint();
	});
	
	popupMenu.add(menuItem);
	
	popupMenu.show(e.getComponent(), e.getX(), e.getY());
}


protected void createPopupMenu(MouseEvent e, Edge edge) {
JMenuItem menuItem; 
	
JPopupMenu popupMenu=new JPopupMenu();
	 
          menuItem = new JMenuItem("Remove this edge");
	      menuItem.addActionListener((a) -> {
	         this.graph.removeEdge(edge);
	         this.repaint();
	      });
	    
  popupMenu.add(menuItem);
 popupMenu.show(e.getComponent(), e.getX(), e.getY());
	
	
}

protected void createPopupMenu(MouseEvent e, ColorEdge edge) {
	JMenuItem menuItem; 
	
	JPopupMenu popupMenu=new JPopupMenu();
	 
          menuItem = new JMenuItem("Remove this edge");
	      menuItem.addActionListener((a) -> {
	         this.graph.removeEdge(edge);
	         this.repaint();
	      });
	    
    popupMenu.add(menuItem);
    
    
        	    menuItem = new JMenuItem("Change color of this edge");
        	         menuItem.addActionListener((a) -> {
        	        	Color newColor=JColorChooser.showDialog(this,"Choose background color", Color.BLUE);
        	     		if(newColor!=null) {
        	     			edge.setColor(newColor);
        	     		}
        	     		repaint();
           
        });
        popupMenu.add(menuItem);
    
	   popupMenu.show(e.getComponent(), e.getX(), e.getY());
	
	
}



}
