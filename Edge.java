package graphEditor;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.Serializable;
//Lilla Lomnicka 238306

public class Edge implements Serializable{
	
	protected Node startNode; 
	protected Node endNode; 
	
	
	public Edge(Node startNode, Node endNode) {
		this.startNode=startNode;
		this.endNode=endNode;
	
	}

	

	public Node getStartNode() {
		return startNode;
	}


	public Node getEndNode() {
		return endNode;
	}

	void draw(Graphics g) {
	    g.setColor(Color.BLACK);
	    g.drawLine(startNode.getX(), startNode.getY(), 
	      endNode.getX(), endNode.getY());
	  }
	
	public boolean isMouseOver(int mouseX, int mouseY) {
		    double Dmin = 2.0D;
		    if (((mouseX > startNode.getX()) && (mouseX > endNode.getX())) || (
		      (mouseX < startNode.getX()) && (mouseX < endNode.getX()))) return false;
		    if (((mouseY > startNode.getY()) && ( mouseY > endNode.getY())) || (
		      (mouseY < startNode.getY()) && (mouseY < endNode.getY()))) return false;
		    int A = endNode.getY() - startNode.getY();
		    int B = startNode.getX() - endNode.getX();
		    int C = endNode.getX() * startNode.getY() - startNode.getX() * endNode.getY();
		    double d = Math.abs(A * mouseX + B * mouseY + C) / Math.sqrt(A * A + B * B);
		    return d <= 2.0D;
		  }
	
	  public String toString()
	  {
	    return "[" + startNode + "==>" + endNode + "]";
	  }
	}

