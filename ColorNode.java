package graphEditor;

import java.awt.Color;
import java.awt.Graphics;
//Lilla Lomnicka 238306

public class ColorNode extends Node{

	
	Color color;
	protected String name; 
	
	public ColorNode(int x, int y) {
		super(x, y);
		this.color=Color.CYAN;
		 this.name="?";
	}

	
	public ColorNode(int x, int y, String name, Color color) {
		super(x, y);
		this.color=color;
		this.name=name;
	}
	
	
	

public Color getColor() {
		return color;
	}


	public void setColor(Color color) {
		this.color = color;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


void draw(Graphics g) {
	
	g.setColor(color); //kolor wypełnienia
	g.fillOval(x-r, y-r, 2*r, 2*r); //wypełnia okrąg
	g.setColor(Color.BLACK); //kolor obramowki
	g.drawOval(x-r, y-r, 2*r, 2*r);
	g.drawString(getName(), x, y);
}

}